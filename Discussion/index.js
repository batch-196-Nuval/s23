console.log("Thank you Lord!");

// JS Objects
// JS Objects are ways to define a real world object with its own characteristics. It is also a way to organize data with context through the use of key-value pairs.

/*
	loyal,
	huggable,
	4 legs,
	furry,
	different colors,
	hyperactive,
	tail
	breed
*/

/*let dog = [

	"loyal",
	4,
	"tail",
	"Husky",

];*/

// describe something with characteristics, use objects

let dog = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true

};

let favMovie = {
	title: "Avengers: Endgame",
	publisher: "Marvel Studios",
	year: 2010,
	director: "Anthony Russo",
	isAvailable: false
}
console.log(favMovie);



// [] - array literals create arrays
// {} - object literals create objects

// Objects are composed of key-value pairs. keys provide lable to your values
// key : value
// each key-value pair together are called properties


// Accessing array items = arrName[index]
// Accessing object protperties = objectName.propertyName

console.log(favMovie.title);

// what if I want to access the publisher?

console.log(favMovie.publisher);

// can we also update the properties of our objects?

// obejct.propertyName = <newValue>
favMovie.year = 2019;
console.log(favMovie);


favMovie.title = "Ang Probinsyano";
favMovie.publisher = "FPJ Productions";
favMovie.year = 1972;

console.log(favMovie);

// Objects can not only have primitive values like strings, numbers, or boolean
// It can also conrain objects and arrays

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]

};

console.log(course);

// can we access the course's instructors array?

console.log(course.instructors);

// access the array item in the object (Mrs. Smith)
console.log(course.instructors[1]);

// can we use the instructor array's array methods? - yes

// delete Mr. Francis

course.instructors.pop();
console.log(course);

// always access the object first, then property name, then method
// objectName.propertyNameArr.method();

course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isAnInstructor = course.instructors.includes("Mr. Johnson");
console.log(isAnInstructor);


//create a function to be able to add new instructors to our object
function addNewInstructor(instructor){

	let isAMember = course.instructors.includes(instructor);
	if (isAMember === true) {
		console.log("Instructor already added.");
	} else {
		course.instructors.push(instructor);
		console.log("Thank you. Instructor added.");
	}

};



/*addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");

console.log(course);
*/

// We can also create / initialize an empty object and then add its properties

let instructor = {};
console.log(instructor);

// if you assign a value to a property that does not yet exist, you are able to add a new property in the object
instructor.name = "James Johnson";

instructor.age = 56;
instructor.gender = "Male";
instructor.department = "Humanities";
instructor.currentSalary = 50000;
instructor.subjects = ["Philosophy", "Humanities", "Logic"];

console.log(instructor);

instructor.address = {
	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}

// how will we acces the street property?

console.log(instructor.address.street);


// Create Objects using a constructor function

// create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves as a blueprint for an object

function Superhero(name, superpower, powerLevel){

	// "this" keyword when added in a constructor function refers to the object that will be made by the function
	/*
		{
			name: <valueOfParameterName>,
			superpower: <valueOfParameterSuperpower>,
			powerLevel: <valueOfParameterPowerLevel>
		}
	*/
	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;

};


// lets create an object out of our Superhero constructor function
// new keyword is added to allow us to create a new object out of our function

let superhero1 = new Superhero("Saitama", "One Punch", 30000);
console.log(superhero1);



function Laptop(name, brand, price){
	this.name = name;
	this.brand = brand;
	this.price = price;
};

let laptop1 = new Laptop("Asus ROG Zephyrus G15", "Asus", 1480);
let laptop2 = new Laptop("Asus Chromebook Flip CX5", "Asus", 723);
console.log(laptop1);
console.log(laptop2);


// Object Methods
// obj methods are functions that are associated with an obj
// A function is a property of an obj and that function belongs to the obj
// Methods are tasks that an obj can peroform or do

// arrayName.method()

let person = {
	name: "Slim Shady",
	talk: function(){

		// methods are functions associated as a property of an obj
		// they are anonymous functions we can invoke using the property of the obj
		/*console.log(this);*/
		// this time, "this" refers to the obj where the method is associated
		console.log("Hi! My name is, What? My name is who? " + this.name);
	}
}

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){

		// greet method should be able to receive a single obj
		console.log("Good day, " + friend.name);
	}
}

person.talk();

person2.greet(person);



// Create a constructor with a built-in method

function Dog(name, breed){

/*	{
		name: <valuePrameterName>,
		breed: <valuePrameterBreed>
	}*/
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}

}

let dog1 = new Dog("Rocky", "Bulldog");
console.log(dog1);
dog1.greet(person);
dog1.greet(person2);


let dog2 = new Dog("Lucky", "Rottweiler");
console.log(dog2);

dog2.greet()