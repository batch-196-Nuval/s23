console.log("To God be the Glory");


// Object Literals
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};

console.log(trainer);
console.log("Result of dot notation: (trainer.name)");
console.log(trainer.name);
console.log("Result of square bracket notation: (trainer.pokemon[0])");
console.log(trainer.pokemon[0]);
console.log("Result of talk method: (trainer.talk)");
trainer.talk();



// Constructor Function
function Pokemon(name, level){
	this.name = name;
	this.level = Number(level);
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(pocketMonster){
		console.log(this.name + " tackled " + pocketMonster.name + "!");
	};
	this.faint = function(){
		console.log(this.name + " has fainted!");
	};
};

let pokemon1 = new Pokemon("Pikachu", "12");
let pokemon2 = new Pokemon("Geodude", "8");
let pokemon3 = new Pokemon("Mewtwo", "100");

console.log("Pokemons created via Constructor Function:");
console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

console.log("Pokemon tackles:");
pokemon1.tackle(pokemon2);
pokemon1.tackle(pokemon3);

pokemon2.tackle(pokemon1);
pokemon2.tackle(pokemon3);

pokemon3.tackle(pokemon1);
pokemon3.tackle(pokemon2);

console.log("Pokemon faints:");
pokemon1.faint();
pokemon2.faint();
pokemon3.faint();



// Stretch Goal
console.log("Stretch Goal:");

function Fighter(name, level){
	this.name = name;
	this.level = Number(level);
	this.health = Number(level * 3);
	this.power = Number(level * 1.5);
	this.attack = function(soldier){
		console.log(this.name + " attacks " + soldier.name + "!");
		console.log(soldier.name + " receives " + this.power + " damage!");
		soldier.health = soldier.health - this.power;
		if (soldier.health <= 0) {
		console.log(soldier.name + "'s health has been reduced to 0. " + soldier.name + " has fainted!");
	}	else {
			console.log(soldier.name + "'s health has been reduced to " + soldier.health + "!");
		}
	};
};

let fighter1 = new Fighter("Squall", "100");
let fighter2 = new Fighter("Seifer", "1");

console.log("Fighters created via Constructor Function:");
console.log(fighter1);
console.log(fighter2);

fighter2.attack(fighter1);
fighter2.attack(fighter1);
fighter2.attack(fighter1);

fighter1.attack(fighter2);